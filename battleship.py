"""
Ту ду:
1) Пока корабли располагаются только горизонтально
2) Дамаг не добавляется при попадении
3) Можно вести счет побед поражений во внешнем файле.
"""


from random import choice


"""
Первые части составных строк необходимые 
для цветной печати в терминале
"""
SEA_COL = '\x1b[5;30;46m'    # Цвет клетки моря (первая часть составной строки)
SHIP_COL = '\x1b[1;46;33m'    # Цвет клетки с попадением в корабль
SHOOT_COL = '\x1b[1;49;46m'    # Цвет клетки промаха


def printer(color):
    """
    Функция принтер. Это замыкание которое позволяет
    создать "каретку" с определённым цветом и минимальной шириной.
    И использовать её в последствии вместо функции print()
    """
    def new_color(word, space=2, m=''):
        word = str(word).rjust(space)       # Создаём строку занимающую минимум space символов
        """ Форматируем строку. подставляем "цвет" + слово,
        всё это кончается второй, необходимой для цвета, частью строки"""
        print('{}{}\x1b[0m'.format(color, word), end=m)
    return new_color


""" Настраиваем наш принтер на разные цвета"""
f_sea = printer(SEA_COL)    # Цвет моря
f_ship = printer(SHIP_COL)  # Цвет корабля
f_shoot = printer(SHOOT_COL)  # Цвет промаха

SIDES = [10, 10]


class Ship(object):
    """
    Класс кораблей.
    Экземплярами этого класса метод player.build_ships
    наполняюет список player.ships.
    Умеет быть на координатах, получать дамаг, тонуть
    """
    def __init__(self,x, y, lenght, orient):
        self.lenght = lenght
        self.orient = orient
        self.damage = 0
        self.x = x
        self.y = y


class Player(object):
    """
    Класс игроков.
    Стреляет, строит свои корабли, считает потери
    """
    def __init__(self, name, score=0, wins=0, looses=0):
        '''
        Инициализация свойств класса.
        Oпределяются в момент создания экземпляра 
        '''
        self.name = name
        self.ships = []             # Будет наполнен методом build_ships 
        self.score = score          # тут всякие разные очки
        self.wins = wins
        self.looses = looses
        self.shoots = []            # Сюда будут добавляться координаты выстрелов 

    def build_ships(self, lens):
        """
        Этот метод строит коробли. 
        Наполняет вышеупомянутый список self.ships экземплярами класса Ship
        Принимает на вход список из чисел.
        Количество чисел определяет количество кораблей,
        а значение этих чисел указывает количество палуб каждого из них
        """
        pos = (0,1)     # Варианты ориентации корабля (0 - гор. 1 - верт)

        ''' Ниже генератором списков наполняем список self.ships
        экземплярами класса Ship. l - длинна борта (кол-во палуб)'''

        self.ships = [Ship(0, 0, l, 0) for l in lens]
        print(self.name+' Ваш флот готов!')

    def shoot(self):
        """
        Стреляем. Принимаем координаты через пробел и добавляем
        их как выстрел в self.shoots.
        """
        shoot = ['x', 'y']
        while len(shoot) != 2 or not shoot[0].isdigit() or not shoot[1].isdigit():
            messege = '\n {},\n введите координаты выстрела \
цифрами через пробел "x y": '.format(self.name)
            shoot = input(messege).split()

        self.shoots.append({'x':int(shoot[0]), 'y':int(shoot[1])})
        self.shoots = sorted(self.shoots, key=lambda shoot: shoot['x'] )

    def analise(self):
        """
        Метод подчета потерь. Переучет живых судов.
        """
        for ship in self.ships:
            if ship.damage >= ship.lenght:      # тут все очевидно
                del(ship)


class Board(object):
    """
    Класс "Доска?)", аля игровое поле.
    Имеет только два свойства - ширина и высота.
    после каждого хода игроков перерисовывает поле.
    Отмечает промахи и попадения.
    """
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def move_ships(self, ships):
        """
        Метод рандомит корабли по полю.
        Gока только горизонтально.
        """
        availible_xs={}
        s = 0
        while s < len(ships):
            ship = ships[s]
            l = ship.lenght
            if not ship.orient:
                if s == 0:
                    x = ship.x = choice(range(self.width - ship.lenght + 1))
                    y = ship.y = choice(range(self.height))

                    availible_xs[y] = list(range(self.width))
                else:
                    y = ship.y = choice(range(self.height))
                    if not availible_xs.get(y):
                        availible_xs[y] = list(range(self.width))[:self.width - ship.lenght + 1]

                    i = 0
                    tmp_list = []
                    while i < len(availible_xs[y]) - 1:
                        if availible_xs[y][i + 1] <= availible_xs[y][i] + l:
                            tmp_list.append(availible_xs[y][i])
                        i += 1
                    x = ship.x = choice(tmp_list)

                if x == 0:
                    del (availible_xs[y][x:l + 1])
                elif ship.x == self.width - l:
                    del (availible_xs[y][x - 1:l])
                else:
                    del (availible_xs[y][x:l])

                if y == 0:
                    availible_xs[y + 1] = availible_xs[y]
                elif y == self.height:
                    availible_xs[y - 1] = availible_xs[y]
                else:
                    availible_xs[y + 1] = availible_xs[y]
                    availible_xs[y - 1] = availible_xs[y]
            # print(ship.x, ship.y)
            s += 1




    def draw_shoots(self, shoots, ships):
        """
        Метод рисует доску для каждого хода.
        для первого игрока она рисует доску с кораблями второго, и наоборот.
        Принимает выстрелы одного игрока и корабли другого.
        В цикле проверяет пересечения и изменяет дамагу.
        """
        print()
        print('   ', end='')
        '''В цикле пишем строку с координатам по Х'''
        for x in range(0, self.width):
            print(str(x)+' ', end='')
        print()

        '''
        Дальше в цикле построчно рисуем пересечения с Х:
            Если только с выстрелом то печатаем промах и 
            переходим на следующий символ по Х (не на строку!)
            Если с выстрелом и кораблём то рисуем попадение
            Если ни с чем то рисуем воду.
        И так для каждой строки (для каждого Y)
        '''
        y = 0
        while y < self.height:
            x = 0
            print(y, end='|')
            while x < self.width:
                old_x = x

                for shoot in shoots:
                    if x == shoot['x'] and y == shoot['y']:

                        for ship in ships:

                            if not ship.orient:  # Если корабль
                                                 # не расположен вертикально

                                if ship.x <= x and \
                                        (ship.x + ship.lenght - 1) >= x \
                                            and ship.y == y:

                                    # ship.damage += 1   # Лупим

                                    f_ship('X')        # Рисуем попадение
                                    # x += ship.lenght                      # Дальше не проверял
                                                                            # Пора спать...zzz
                                    x += 1

                            else:
                                pass
                                """if ship.x == x and ship.y >= y and (ship.y + ship.lenght - 1) >= y:
                                    ship.damage += 1
                                    f_ship('X')       
                                    x += 1"""

                        if old_x == x:
                            f_shoot(' ~', 2)
                            x += 1

                if x < self.width and old_x == x:
                    f_sea(' ~', 2)
                    x += 1
            print()
            y += 1


def game(lens):      

    p1 = Player(input('Игрок 1, представься!: '))   # Создаём экземпляр класса Player
    p1.build_ships(lens)                            # он создаёт себе флот

    p2 = Player(input('Игрок 2, представься!: '))   # Создаём ещё экземпляр класса Player
    p2.build_ships(lens)                            # он создаёт себе флот

    b = Board(*SIDES)                               # Создаём экз. кл. Board

    b.draw_shoots(p1.shoots, p2.ships)              # Board рисует чистое поле

    b.move_ships(p1.ships)                          # метод который расположит
    b.move_ships(p2.ships)                          # корабли по полю рандомно

    while p1.ships and p2.ships:            # пока у кого-либо не кончатся корабли...
        print('\n ход игрока 1')
        b.draw_shoots(p1.shoots, p2.ships)  # доска показывает прошлые выстрелы
        p1.shoot()                          # player1 стреляет
        b.draw_shoots(p2.shoots, p1.ships)  # доска перерисовывается
        p2.analise()                        # player2 проверяет потери

        print('\n ход игрока 2')
        b.draw_shoots(p1.shoots, p2.ships)  # тоже само для второго игрока
        p2.shoot()                          # ...
        b.draw_shoots(p2.shoots, p1.ships)
        p1.analise()

    print('G A M E  O V E R')               # если корабли кончились.


lens = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1]       # Указываем какими кораблями наполнять флот,
                             # значение цифры определяе количество палуб

game(lens)                   # запускаем игру
